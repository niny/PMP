# Client-side validation

- Client-side validation is an initial check and an important feature of good user experience; by catching invalid data on the client-side, the user can fix it straight away. If it gets to the server and is then rejected, a noticeable delay is caused by a round trip to the server and then back to the client-side to tell the user to fix their data

### There are two different types of client-side validation:

- 1. Built-in form validation uses HTML5 form validation features. Built-in form validation has better performance than JavaScript, but it is not as customizable as JavaScript validation.
- 2. JavaScript validation is coded using JavaScript. This validation is completely customizable, but you need to create it all (or use a library).

### HTML5 built-in form validation using attributes on form elements
- required: Specifies whether a form field needs to be filled in before the form can be submitted.
- minlength and maxlength: Specifies the minimum and maximum length of textual data (strings)
- min and max: Specifies the minimum and maximum values of numerical input types
- type: Specifies whether the data needs to be a number, an email address, or some other specific preset type. 
- pattern: Specifies a regular expression that defines a pattern the entered data needs to follow.
Note:- HTML5 Constraint validation doesn't remove the need for validation on the server side.

#### In HTML5, basic constraints are declared in two ways:

- By choosing the most semantically appropriate value for the type attribute of the <input> element, e.g., choosing the email type automatically creates a constraint that checks whether the value is a valid e-mail address.
- By setting values on validation-related attributes, allowing basic constraints to be described in a simple way, without the need for JavaScript.


For more information -->> https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Constraint_validation



