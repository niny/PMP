<html>
<head>
		<title>Example Organization</title>
		<link href="/styles/style.css" rel="stylesheet">
	</head>
    <body>
        <div class="container">
            <a href="/">Home</a>
            <h3>List Users</h3>
<?php
    include "db.php";

    $query = "SELECT `id`, `fname`, `lname`, `email` FROM users";

    # execute the query
if ($result = mysqli_query($mysqli, $query)) {
if (mysqli_num_rows($result) > 0) {

    echo "<ul class='table'>";
    echo "<li class='header'>
              <span style='width:5%'>ID</span>
              <span style='width:25%'>Name</span>
              <span style='width:35%'>Email</span>
              <span>&nbsp;</span>
          </li>";
    while($row = $result->fetch_object()) {
        echo "<li>
                <span style='width:5%'>$row->id</span>
                <span style='width:25%'>$row->fname $row->lname</span>
                <span style='width:35%'>$row->email</span>
                <span><a href='update_user.php?id=$row->id'>Edit</a></span>
              </li>";
    }
    echo "</ul>";
    
    $result->close();
}
} else {
    echo "Something went wrong!";
} 
?>
        </div>
    </body>
</html>