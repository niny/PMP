<!DOCTYPE HTML>
<html>
	<head>
		<title>Example Organization</title>
		<link href="/styles/style.css" rel="stylesheet">
	</head>
	<body>
		<div>
            <ul>
                <li>
                    <h3>PMP</h3>
                    <a style="float:right;" href="/">Home</a>
                </li>
            </ul>
            <?php

                include "db.php";

                $id = $_GET["id"];

                if ($id) {

                    $query = "SELECT * FROM users WHERE id = $id";

                    $result = mysqli_query($mysqli, $query);

                    if (mysqli_num_rows($result) > 0) {
                        $user = $result->fetch_object();
                    }

                }

            ?>
			<form action="/form_collect.php" method="post">
                <ul>
                    <li><h3>Update User form</h3><li>
                    <li>
                        <label for="name">First Name (upper and lowercase letters):</label>
                        <input type="text" id="fname" name="fname" required pattern="[A-Za-z]+" value="<?php echo $user->fname;?>" placeholder="Your First Name" />
                    </li>

                    <li>
                        <label for="name">Last Name (upper and lowercase letters):</label>
                        <input type="text" id="lname" name="lname" required pattern="[A-Za-z]+" value="<?php echo $user->lname;?>" placeholder="Your Last Name" />
                    <li>
                        <label for="email">Email:</label>
                        <input type="email" id="email" name="email" value="<?php echo $user->email;?>" placeholder="Your E-mail id" />
                    </li>

                    <li>
                        <label for="phone">Phone:</label>
                        <input type="tel" id="phone" name="phone" value="<?php echo $user->phone;?>" pattern="[0-9]{6,15}">
                    </li>

                    <li>
                        <label for="age">Age:</label>
                        <input type="number" id="age" name="age" value="<?php echo $user->age;?>" min="1" max="120">
                    </li>

                    <li>
                        <label>&nbsp;</label>
                        <input type="hidden" name="user_id" value="<?php echo $id?>">
                        <input type="hidden" name="mode" value="update">
                        <input class="submit" type="submit" value="Submit">
                        <input class="reset" type="reset" value="Reset">
                    </li>
                </ul>
			</form>
		</div>
	</body>
</html>

