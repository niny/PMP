# Requirements:-

- 1. The web form should follow a logical and standard sequence, which uses interface design principles.

- 2. Automated processing should be used within the interface through the use of HTML5 constraints, which checks the user’s input before submitting the form.

- 3. When the form is submitted by the user your PHP code will add the incoming values (from the form) to the SQL string, which will add the data to the database.

- 4. The database, which you will create, should match the computational map (ERD) and apply all properties and attributes, which are identified within the entity.

- 5. Your web form must allow the user to view information (SELECT) add new information (INSERT INTO) and update stored information (UPDATE).  
