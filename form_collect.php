<?php

# this function helps to print any variable; you can remove after use
function stop($var) {
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    exit;
}

// The global $_POST variable allows you to access the data sent with the POST method by name
$data = $_POST;

$mode = $_POST["mode"];

if ($mode == 'update') {
    $id = $_POST['user_id'];
;}

# checking mandatory fields in server
if (!$data["fname"]) {
    echo "First name should not blank<br>";
    $error = true;
}

if (!$data["lname"]) {
    echo "Last name should not blank<br>";
    $error = true;
}

if (!$data["email"]) {
    echo "email should not blank";
    $error = true;
}

# setting link to form page
if ($error) {
    if ($mode == "insert") {
        
        echo "<br><a href='/'>Back</a>";
    } else {
        
        echo "<br><a href='/update_user?id=$id'>Back</a>";
    }
    exit;
}

include "db.php";

# preventing SQL injection
$fname = mysqli_real_escape_string($mysqli, htmlspecialchars($data["fname"]));
$lname = mysqli_real_escape_string($mysqli, htmlspecialchars($data["lname"]));
$email = mysqli_real_escape_string($mysqli, htmlspecialchars($data["email"]));

$phone = $data["phone"];
$age = $data["age"];

# build the insert query

if ($mode == "insert") {

    $query = sprintf("INSERT INTO users(fname, lname, email, phone, age, created_on) VALUES('%s', '%s', '%s', '%s', %d, now())", 
    $fname, $lname, $email, $phone, $age);
} else {
    $query = sprintf("UPDATE users set fname = '%s', lname = '%s', email = '%s', phone = '%s', age = %d WHERE id = $id", 
    $fname, $lname, $email, $phone, $age);
    echo $query;
}

# execute the query
if (mysqli_query($mysqli, $query)) {
    if ($mode == 'insert') $msg = "User added successfully.";
    else $msg = "User updated successfully.";
    echo $msg;
    echo "<br><a href='/'>Home</a>";
    echo "<br><a href='/list_users.php'>List Users</a>";
} else {
    echo "Something went wrong!";
}


?>
