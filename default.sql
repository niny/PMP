CREATE TABLE users(
    id smallint PRIMARY KEY AUTO_INCREMENT,
    fname varchar(100) not null,
    lname varchar(100) not null,
    email varchar(255) not null,
    age smallint,
    phone varchar(20),
    created_on datetime not null,
    last_updated_on datetime not null default now()
);
